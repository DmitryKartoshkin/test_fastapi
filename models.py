from sqlalchemy import Table, Column, Integer, String, create_engine, ForeignKey
from database import metadata, DATABASE_URL


peoples = Table(
    "peoples",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(length=150), nullable=False, unique=True),
    Column("gender", String(length=30), nullable=False),
)


engine = create_engine(DATABASE_URL)
metadata.create_all(engine)