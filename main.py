from typing import List
from fastapi import FastAPI
from database import database
from filter import _filter_set
from models import peoples
from schemas import People, PeopleIn


app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/user/", response_model=List[People])
async def read_all():
    """Функция для получения всех данных всех пользователей"""
    query = peoples.select()
    response = await database.fetch_all(query)
    return response


@app.get("/user/limit/", response_model=List[People])
async def read_limit(skip: int = 0, limit: int = 3):
    """Функция для получения всех данных всех пользователей
       согласно установленного лимита"""
    query = peoples.select()
    response = await database.fetch_all(query)
    return response[skip: skip + limit]


@app.get("/user/{user_gender}", response_model=List[People])
async def read_filter_gender(user_gender: str):
    """Функция для получения всех данных всех пользователей
        согласно гендера"""
    query = peoples.select()
    response = await database.fetch_all(query)
    sort_response = _filter_set(response, user_gender)
    return sort_response


@app.post("/user/", response_model=People)
async def create(people: PeopleIn):
    """Функция для создания записи о пользователе в БД"""
    query = peoples.insert().values(name=people.name, gender=people.gender)
    last_record_id = await database.execute(query)
    return {**people.dict(), "id": last_record_id}

