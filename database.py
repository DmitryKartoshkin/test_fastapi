import databases
import sqlalchemy

from settings import DIALECT, USER_NAME, PASSWORD, HOST, DATA_BASE


DATABASE_URL = f"{DIALECT}://{USER_NAME}:{PASSWORD}@{HOST}/{DATA_BASE}"

database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()
# engine = sqlalchemy.create_engine(DATABASE_URL)
# metadata.create_all(engine)

