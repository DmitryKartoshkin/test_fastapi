from pydantic import BaseModel


class People(BaseModel):
    id: int
    name: str
    gender: str


class PeopleIn(BaseModel):
    name: str
    gender: str


