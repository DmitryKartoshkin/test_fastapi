import os
import dotenv

dotenv.load_dotenv('.env')

DIALECT = os.environ["DIALECT"]
USER_NAME = os.environ["USER_NAME"]
PASSWORD = os.environ["PASSWORD"]
HOST = os.environ["HOST"]
DATA_BASE = os.environ["DATA_BASE"]
